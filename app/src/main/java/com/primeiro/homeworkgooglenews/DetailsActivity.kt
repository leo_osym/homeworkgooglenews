package com.primeiro.homeworkgooglenews

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_details.*

class DetailsActivity : AppCompatActivity() {

    var entry_id = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        getActionBar()?.setDisplayHomeAsUpEnabled(true)

        var entries = Entries.All

        if(intent.hasExtra("ID")){
            entry_id = intent.getIntExtra("ID",0)
            details_title.text = entries[entry_id].Title
            details_content.text = entries[entry_id].Content
        }
    }
}
