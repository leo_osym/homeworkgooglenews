package com.primeiro.homeworkgooglenews

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.inputmethod.InputMethodManager
import com.koushikdutta.ion.Ion
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    private val API_KEY = "b34f14127ae348478d1d9b1eb7292375"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // if results is already exist
        loadContent()

        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        //make sure search_field is visible (not overlapped by keyboard)
        imm.showSoftInput(search_field, InputMethodManager.SHOW_IMPLICIT)

        button_search.setOnClickListener {
            // hide keyboard
            imm.hideSoftInputFromWindow(it.getWindowToken(), 0)

            val query = search_field.text.toString().replace(" ","+")
            val urlString = "https://newsapi.org/v2/everything?q=$query&sortBy=date&apiKey=$API_KEY"

            Entries.All.clear()
            recycler_news_list.removeAllViews()
            processQuery(urlString)
        }
    }

    fun processQuery(url: String){
        Ion.with(this)
            .load(url)
            .asString()
            .setCallback { _, result ->
                Log.i("JSON", result)

                var articles = JSONArray()
                try{
                    articles = JSONObject(result).getJSONArray("articles")
                } catch(ex: Exception) {
                    Log.e("JSONObject","Can't parse JSON object! "+ex.message)
                }

                if(articles.length() > 0) {
                    for (i in 0..(articles.length() - 1)) {
                        val title = articles.getJSONObject(i).getString("title")
                        val content = articles.getJSONObject(i).getString("content")
                        Entries.All.add(Entry(title,content))
                    }
                }
                loadContent()
            }
    }

    fun loadContent(){
        val adapter = EntryAdapter(Entries.All, this)
        // add separator line
        recycler_news_list.addItemDecoration(
            DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        recycler_news_list.layoutManager = LinearLayoutManager(this)

        adapter.setListener(object : EntryAdapter.Listener {
            override fun onClick(position: Int) {
                val intent = Intent(baseContext, DetailsActivity::class.java)
                intent.putExtra("ID", position)
                startActivity(intent)
            }
        })
        recycler_news_list.adapter = adapter
    }
}
