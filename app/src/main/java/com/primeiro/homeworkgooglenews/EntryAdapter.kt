package com.primeiro.homeworkgooglenews

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class EntryAdapter(val items : ArrayList<Entry>, val context: Context):
    RecyclerView.Adapter<EntryAdapter.ViewHolder>() {

    // add interface-listener to move intent functionality to MainActivity
    private lateinit var listener: Listener
    interface Listener {
        fun onClick(position: Int)
    }

    fun setListener(listener: Listener) {
        this.listener = listener
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title?.text = items.get(position).Title
        holder.content?.text = items.get(position).Content

        holder.itemView.setOnClickListener{
            listener.onClick(position)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context)
            .inflate(R.layout.entry_template, parent, false))
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val title = itemView.findViewById<TextView>(R.id.entry_title)
        val content = itemView.findViewById<TextView>(R.id.entry_content)
    }
}